package com.example.homework3.service.serviceImp;

import com.example.homework3.exception.AuthorNotFoundException;
import com.example.homework3.exception.InvalidException;
import com.example.homework3.model.entity.Author;
import com.example.homework3.model.request.AuthorRequest;
import com.example.homework3.repository.AuthorRepository;
import com.example.homework3.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService {

    private  final AuthorRepository  authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        if(authorRepository.getAuthorById(authorId) != null ){
            return authorRepository.getAuthorById(authorId);
        }else throw new AuthorNotFoundException(authorId);
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {
        if(authorRequest.getAuthor_name().equalsIgnoreCase("null")){
            throw new InvalidException("Author Name Is Can't Null");
        }else if(authorRequest.getAuthor_name().isEmpty()){
            throw new InvalidException("Author Name Is Can't Empty");
        }else if(authorRequest.getAuthor_name().isBlank()){
            throw  new InvalidException("Author Name Is Can't Blank");
        }
        if(!(authorRequest.getGender().equalsIgnoreCase("male"))  && !(authorRequest.getGender().equalsIgnoreCase("female"))){
            throw  new InvalidException("Invalid Field, Please Input Male or Female");
        }
        authorRequest.setGender(authorRequest.getGender().toLowerCase());
        return authorRepository.addNewAuthor(authorRequest);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        if(authorRepository.getAuthorById(authorId) != null){
            return authorRepository.deleteAuthor(authorId);
        }else throw new AuthorNotFoundException(authorId);

    }

    @Override
    public Integer updateAuthorById(AuthorRequest authorRequest, Integer authorId) {
        if(authorRepository.getAuthorById(authorId) != null){
            if(authorRequest.getAuthor_name().equalsIgnoreCase("null")){
                throw new InvalidException("Author Name Is Can't Null");
            }else if(authorRequest.getAuthor_name().isEmpty()){
                throw new InvalidException("Author Name Is Can't Empty");
            }else if(authorRequest.getAuthor_name().isBlank()){
                throw  new InvalidException("Author Name Is Can't Blank");
            }
            if(!(authorRequest.getGender().equalsIgnoreCase("male"))  && !(authorRequest.getGender().equalsIgnoreCase("female"))){
                throw  new InvalidException("Invalid Field, Please Input Male or Female");
            }
            authorRequest.setGender(authorRequest.getGender().toLowerCase());
            return authorRepository.updateAuthorById(authorRequest,authorId);
        }else throw new AuthorNotFoundException(authorId);
    }


}
