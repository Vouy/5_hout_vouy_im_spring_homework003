package com.example.homework3.service.serviceImp;

import com.example.homework3.exception.BookNotFoundException;
import com.example.homework3.exception.CategoryNotFoundException;
import com.example.homework3.exception.InvalidException;
import com.example.homework3.model.entity.Book;
import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.BookRequest;
import com.example.homework3.repository.BookRepository;
import com.example.homework3.repository.CategoryRepository;
import com.example.homework3.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private  final BookRepository bookRepository;

    private  final CategoryRepository  categoryRepository;

    public BookServiceImp(BookRepository bookRepository, CategoryRepository categoryRepository) {
        this.bookRepository = bookRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Book addNewBook(BookRequest bookRequest) {

        if(bookRequest.getTitle().equalsIgnoreCase("null")){
            throw new InvalidException("Book Title Can't Null");
        }else if(bookRequest.getTitle().isEmpty()){
            throw new InvalidException("Book Title Can't Empty");
        }else if(bookRequest.getTitle().isBlank()){
            throw new InvalidException("Book Title Can't Blank");
        }

        for (Integer  categoryId:bookRequest.getCategory_id()){
            Category c =  categoryRepository.getCategoryById(categoryId);
            if(c == null){
               throw new CategoryNotFoundException(categoryId);
            }
        }

        Integer bookID = bookRepository.addNewBook(bookRequest);
        for(Integer categoryId : bookRequest.getCategory_id()){
            bookRepository.addBookDetails(bookID,categoryId);
        }
        return bookRepository.getBookById(bookID);
    }

    @Override
    public Book getBookById(Integer bookId) {
        if(bookRepository.getBookById(bookId) != null){
            return bookRepository.getBookById(bookId);
        }else  throw new BookNotFoundException(bookId);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBook();
    }

//    @Override
//    public Integer updateBookTITle(String name, Integer id) {
//        return bookRepository.updateBookTITle(name,id);
//    }

    @Override
    public boolean deleteBookById(Integer bookId) {
        if(bookRepository.getBookById(bookId) != null){
            return bookRepository.deleteBookById(bookId);
        }else throw new BookNotFoundException(bookId);

    }

    @Override
    public Integer updateBook(BookRequest bookRequest, Integer bookId) {

        if(bookRequest.getTitle().equalsIgnoreCase("null")){
            throw new InvalidException("Book Title Can't Null");
        }else if(bookRequest.getTitle().isEmpty()){
            throw new InvalidException("Book Title Can't Empty");
        }else if(bookRequest.getTitle().isBlank()){
            throw new InvalidException("Book Title Can't Blank");
        }

        for (Integer  categoryId:bookRequest.getCategory_id()){
            Category c =  categoryRepository.getCategoryById(categoryId);
            if(c == null){
                throw new CategoryNotFoundException(categoryId);
            }
        }
        if(bookRepository.getBookById(bookId) == null){
            throw new BookNotFoundException(bookId);
        }else{

            bookRepository.deleteCategoryBeforeUpdate(bookId);
            for(Integer categoryId : bookRequest.getCategory_id()){
                bookRepository.addBookDetails(bookId,categoryId);
            }
            Integer id = bookRepository.updateBook(bookRequest,bookId);
            return id;
        }

    }

}
