package com.example.homework3.service.serviceImp;

import com.example.homework3.exception.CategoryNotFoundException;
import com.example.homework3.exception.InvalidException;
import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.CategoryRequest;
import com.example.homework3.repository.CategoryRepository;
import com.example.homework3.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private  final CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId) != null){
            return categoryRepository.getCategoryById(categoryId);
        }else throw new CategoryNotFoundException(categoryId);

    }

    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest) {
        if(categoryRequest.getCategory_name().equalsIgnoreCase("null")){
            throw new InvalidException("Category Name Can't Null");
        }else if(categoryRequest.getCategory_name().isEmpty()){
            throw new InvalidException("Category Name Is Can't Empty ");
        }else  if(categoryRequest.getCategory_name().isBlank()){
            throw new InvalidException("Category Name Is Can't Blank ");
        }
        return categoryRepository.addNewCategory(categoryRequest);
    }

    @Override
    public boolean deleteCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId) != null){
            return categoryRepository.deleteCategoryById(categoryId);
        }else throw new CategoryNotFoundException(categoryId);

    }

    @Override
    public Integer updateCategoryById(CategoryRequest categoryRequest, Integer categoryId){
        if(categoryRepository.getCategoryById(categoryId) != null){
            if(categoryRequest.getCategory_name().equalsIgnoreCase("null")){
                throw new InvalidException("Category Name Can't Null");
            }else if(categoryRequest.getCategory_name().isEmpty()){
                throw new InvalidException("Category Name Is Can't Empty ");
            }else  if(categoryRequest.getCategory_name().isBlank()){
                throw new InvalidException("Category Name Is Can't Blank ");
            }

            return categoryRepository.updateCategoryById(categoryRequest,categoryId);
        }else  throw  new CategoryNotFoundException(categoryId);

    }
}
