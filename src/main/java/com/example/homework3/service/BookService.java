package com.example.homework3.service;

import com.example.homework3.model.entity.Book;
import com.example.homework3.model.request.BookRequest;

import java.util.List;

public interface BookService {

    Book  addNewBook(BookRequest bookRequest);

    Book  getBookById(Integer bookId);

    List<Book> getAllBooks();

//    Integer updateBookTITle(String name, Integer id);

    public boolean deleteBookById(Integer bookId);

    Integer updateBook(BookRequest bookRequest, Integer bookId);

}
