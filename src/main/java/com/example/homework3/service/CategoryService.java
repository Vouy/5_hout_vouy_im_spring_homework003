package com.example.homework3.service;

import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategory();

    Category  getCategoryById(Integer categoryId);

    Integer  addNewCategory(CategoryRequest   categoryRequest);

    boolean  deleteCategoryById(Integer categoryId);

    Integer updateCategoryById(CategoryRequest  categoryRequest, Integer categoryId);
}
