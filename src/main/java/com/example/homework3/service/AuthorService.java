package com.example.homework3.service;

import com.example.homework3.model.entity.Author;
import com.example.homework3.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();

    public Author getAuthorById(Integer authorId);

    Integer addNewAuthor(AuthorRequest authorRequest);

    boolean deleteAuthorById(Integer authorId);

    Integer updateAuthorById(AuthorRequest authorRequest,Integer authorId);

}
