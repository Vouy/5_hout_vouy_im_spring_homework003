package com.example.homework3.repository;

import com.example.homework3.model.entity.Author;
import com.example.homework3.model.request.AuthorRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AuthorRepository {
    @Select("SELECT * FROM authors ORDER BY author_id ASC ")
    List<Author> getAllAuthor();

    @Select("SELECT * FROM authors WHERE author_id = #{authorId}")
    public Author getAuthorById(Integer authorId);

    @Select("INSERT INTO authors (author_name,gender)  VALUES(#{request.author_name},#{request.gender}) RETURNING author_id")
    Integer addNewAuthor(@Param("request") AuthorRequest authorRequest);

    @Delete("DELETE FROM  authors WHERE author_id = #{authorId}")
    boolean  deleteAuthor(Integer authorId);

    @Select("UPDATE  authors " +
            "SET author_name = #{request.author_name}, " +
            "gender = #{request.gender} " +
            "WHERE author_id = #{authorId} " +
            "RETURNING  author_id")
    Integer updateAuthorById(@Param("request") AuthorRequest authorRequest,Integer authorId);
}
