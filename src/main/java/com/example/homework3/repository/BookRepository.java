package com.example.homework3.repository;

import com.example.homework3.model.entity.Book;
import com.example.homework3.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("INSERT INTO books (published_date, title , author_id) VALUES(#{request.date},#{request.title}, #{request.author_id}) " +
            "RETURNING id")
    Integer addNewBook(@Param("request") BookRequest bookRequest);

    @Select("INSERT INTO book_details (book_id , category_id) VALUES(#{bookID}, #{categoryID})")
    Integer addBookDetails(Integer bookID, Integer categoryID);

    @Select("SELECT * FROM books  WHERE id = #{bookId}")
    @Result(property = "id" , column = "id")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.example.homework3.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categoryList", column = "id",
            many = @Many(select = "com.example.homework3.repository.CategoryRepository.getCategoryByBookId")
    )
    Book getBookById(Integer invoiceId);

    @Select("SELECT * FROM books ORDER BY id ASC")
    @Result(property = "id" , column = "id")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.example.homework3.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categoryList", column = "id",
            many = @Many(select = "com.example.homework3.repository.CategoryRepository.getCategoryByBookId")
    )
    List<Book> getAllBook();

    @Delete("DELETE FROM books  WHERE id = #{bookId} ")
    boolean deleteBookById(Integer bookId);

    @Select("UPDATE  books " +
            "SET published_date = #{request.date}, " +
            "author_id = #{request.author_id}, " +
            "title = #{request.title} " +
            "WHERE  id = #{bookId} RETURNING id")
    Integer updateBook(@Param("request") BookRequest  bookRequest ,Integer bookId);

//    @Select("UPDATE  book_details " +
//            "SET category_id = #{category_id} " +
//            "WHERE book_id =  #{bookId} ")
//    void updateBookIdByBookIdInTableBookDetail(Integer bookId, Integer category_id);

    @Delete("DELETE  FROM  book_details WHERE book_id = #{bookId}")
    void deleteCategoryBeforeUpdate(Integer categoryId);

//    @Select("update books set title = #{name} where id = #{id}")
//    Integer updateBookTITle(String name, Integer id);
}
