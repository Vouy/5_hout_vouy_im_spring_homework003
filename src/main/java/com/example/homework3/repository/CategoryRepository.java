package com.example.homework3.repository;

import com.example.homework3.model.entity.Book;
import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.CategoryRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("SELECT * FROM categories ORDER BY id ASC")
    List<Category>  getAllCategory();

    @Select("SELECT * FROM  categories WHERE id = #{categoryId}")
    Category getCategoryById(Integer categoryId);

    @Select("INSERT  INTO categories(category_name) VALUES(#{request.category_name}) RETURNING id")
    Integer  addNewCategory(@Param("request")  CategoryRequest categoryRequest);

    @Delete("DELETE  FROM categories WHERE id = #{categoryId}")
    boolean  deleteCategoryById(Integer categoryId);

    @Select("UPDATE categories " +
            "SET category_name = #{request.category_name} " +
            "WHERE id = #{categoryId} " +
            "RETURNING id")
    Integer updateCategoryById(@Param("request") CategoryRequest categoryRequest,Integer categoryId);


    @Select("SELECT DISTINCT c.id , c.category_name FROM categories c " +
                "INNER JOIN book_details  d ON c.id = d.category_id " +
            "WHERE d.book_id = #{bookId}")
    List<Category> getCategoryByBookId(Integer bookId);
}
