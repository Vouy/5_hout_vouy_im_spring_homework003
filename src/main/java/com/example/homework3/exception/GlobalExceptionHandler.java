package com.example.homework3.exception;

import com.example.homework3.model.entity.Category;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BookNotFoundException.class)
    ProblemDetail  handlerBookNotFoundException(BookNotFoundException e){
        ProblemDetail  problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setTitle("Book Not Found Exception");
        problemDetail.setDetail(" Book ID Not Found");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/books/not-found"));
        return problemDetail;
    }

    @ExceptionHandler(InvalidException.class)
    ProblemDetail  handlerFileInputNotFoundException(InvalidException e){
        ProblemDetail  problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
        problemDetail.setTitle("Book Not Found Exception");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/books/bad-request"));
        return problemDetail;
    }


    @ExceptionHandler(AuthorNotFoundException.class)
    ProblemDetail  handlerAuthorNotFoundException(AuthorNotFoundException e){
        ProblemDetail  problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setTitle("Author Not Found Exception");
        problemDetail.setDetail("Author ID Not Found");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/author/not-found"));
        return problemDetail;
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    ProblemDetail  handlerCategoryNotFoundException(CategoryNotFoundException e){
        ProblemDetail  problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setTitle("Category Not Found Exception");
        problemDetail.setDetail(" Category ID Not Found");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/category/not-found"));
        return problemDetail;
    }
}
