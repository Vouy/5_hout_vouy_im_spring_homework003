package com.example.homework3.exception;

public class CategoryNotFoundException extends RuntimeException{
    public CategoryNotFoundException(Integer categoryId){
        super(" Category ID : "+categoryId+"");
    }
}
