package com.example.homework3.exception;

public class AuthorNotFoundException extends RuntimeException{
    public  AuthorNotFoundException(Integer AuthorId){
        super("Author ID : "+AuthorId+"");
    }
}
