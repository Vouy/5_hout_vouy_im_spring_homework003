package com.example.homework3.exception;

public class InvalidException extends RuntimeException{
    public InvalidException(String message){
        super(message);
    }
}
