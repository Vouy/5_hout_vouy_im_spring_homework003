package com.example.homework3.exception;

public class BookNotFoundException extends RuntimeException{
    public BookNotFoundException(Integer bookId){
        super("Book with Id : "+bookId+"");
    }

}
