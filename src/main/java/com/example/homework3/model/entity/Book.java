package com.example.homework3.model.entity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Book {
    private  Integer  id;
//    @NotBlank(message = "Book Tittle Can't Blank")
//    @NotNull(message = "Book Tittle Can't Null")
//    @NotEmpty(message = "Book Tittle Can't Empty String")
    private  String   title;
    private  String  published_date;
    private  Author author;
    private List<Category> categoryList;
}
