package com.example.homework3.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class BookRequest {
    private  Timestamp date ;
    private  String  title;
    private  Integer  author_id;
    private  ArrayList<Integer> category_id ;
}
