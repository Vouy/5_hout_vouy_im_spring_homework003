package com.example.homework3.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonPropertyOrder({"timestamp", "httpStatus", "message","payload"})
public class AuthorResponse<T> {

    private  String  message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private  T payload;
    private  int  httpStatus;
    private Timestamp timestamp;
}
