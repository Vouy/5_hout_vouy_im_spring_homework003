package com.example.homework3.controller;

import com.example.homework3.model.entity.Author;
import com.example.homework3.model.entity.Book;
import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.BookRequest;
import com.example.homework3.model.response.BookResponse;
import com.example.homework3.service.AuthorService;
import com.example.homework3.service.BookService;
import com.example.homework3.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/api/v1/book")
public class BookController {
    private  final BookService bookService;
    private  final CategoryService  categoryService;


    public BookController(BookService bookService, CategoryService categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }

    @PostMapping("/add-book")
    @Operation(summary = "Add Book")
    public ResponseEntity<?> addNewBook (@RequestBody BookRequest bookRequest){
//        for (Integer  categoryId:bookRequest.getCategory_id()){
//            Category c =   categoryService.getCategoryById(categoryId);
//            if(c == null){
//                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This Category ID "+categoryId+" Is Not Available");
//            }
//        }

        Book storeBookId = bookService.addNewBook(bookRequest);
        if(storeBookId != null){
            BookResponse<Book> response = BookResponse.<Book>builder()
                    .message("Add Book Successfully")
                    .payload(storeBookId)
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return  null;
    }

    @GetMapping("/get-book-by-id/{bookId}")
    @Operation(summary = "Get Book By Id")
    ResponseEntity<BookResponse<Book>> getBookById(@PathVariable("bookId") Integer bookId){
        if(bookService.getBookById(bookId) != null){
            BookResponse<Book> response = BookResponse.<Book>builder()
                    .message("Successfully fetched books")
                    .payload(bookService.getBookById(bookId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    @GetMapping("/get-all-book")
    @Operation(summary = "Get All Book")
    ResponseEntity<BookResponse<List<Book>>> getAllBook(){
        BookResponse<List<Book>> response =  BookResponse.<List<Book>>builder()
                .message("Get Data Successfully")
                .payload(bookService.getAllBooks())
                .httpStatus(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete-book-by-id/{bookId}")
    @Operation(summary = "Delete Book By ID")
    public ResponseEntity<BookResponse<String>> deleteBookById(@PathVariable("bookId") Integer bookId){
        BookResponse<String> response = null;
        if(bookService.deleteBookById(bookId) ==  true){
            response = BookResponse.<String>builder()
                    .message("Delete Book Successfully")
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
        return null;

    }

//    @PutMapping("/update/{id}")
//    public ResponseEntity<?> updateBookTitle(@RequestBody String name, @PathVariable("id") Integer id){
//        Integer update =  bookService.updateBookTITle(name,id);
//        if(update != null){
//            BookResponse<Book> response = BookResponse.<Book>builder()
//                    .message("Update Book By Id Successfully")
//                    .payload(bookService.getBookById(update))
//                    .httpStatus(HttpStatus.OK.value())
//                    .timestamp(new Timestamp(System.currentTimeMillis()))
//                    .build();
//            return ResponseEntity.ok(response);
//        }else {
//            System.out.println("Hello");
//             throw new BookNotFoundException(id);
//        }
//    }

    @PutMapping("/update-book-by-id/{bookId}")
    @Operation(summary = "Update Book By ID")
    public ResponseEntity<?> updateBook(@RequestBody BookRequest bookRequest ,@PathVariable("bookId") Integer bookId){

        Integer update =  bookService.updateBook(bookRequest,bookId);
        System.out.println(bookService.getBookById(update));
        if(update != null){
            BookResponse<Book> response = BookResponse.<Book>builder()
                    .message("Update Book By Id Successfully")
                    .payload(bookService.getBookById(update))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;

    }


}
