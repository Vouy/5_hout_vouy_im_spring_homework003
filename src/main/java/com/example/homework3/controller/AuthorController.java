package com.example.homework3.controller;

import com.example.homework3.exception.AuthorNotFoundException;
import com.example.homework3.model.entity.Author;
import com.example.homework3.model.request.AuthorRequest;
import com.example.homework3.model.response.AuthorResponse;
import com.example.homework3.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/author")
public class AuthorController {

    private final AuthorService  authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/get-all-author")
    @Operation(summary = "Get All Author")
    ResponseEntity<AuthorResponse<List<Author>>> getAllAuthor(){
        AuthorResponse<List<Author>> response  = AuthorResponse.<List<Author>>builder()
                .payload(authorService.getAllAuthor())
                .message("Successfully fetched authors")
                .httpStatus(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response);
    }

    @GetMapping("/get-author-by-id/{authorId}")
    @Operation(summary = "Get Author By Id")
    ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("authorId") Integer authorId){
        if (authorService.getAuthorById(authorId) != null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .message("Successfully fetched author")
                    .payload(authorService.getAuthorById(authorId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
        return null;
    }

    @PostMapping("/add-new-author")
    @Operation(summary = "Add New Author")
    ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){
        Integer storeAuthorId = authorService.addNewAuthor(authorRequest);
        if(storeAuthorId != null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .message("Successfully added author")
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @DeleteMapping("/delete-author-by-id/{authorId}")
    @Operation(summary = "Delete Author By Id ")
    ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("authorId") Integer authorId){
        AuthorResponse<String> response =  null;
        if(authorService.deleteAuthorById(authorId) == true){
            response = AuthorResponse.<String>builder()
                    .message("Delete Author Successfully")
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;

    }

    @PutMapping("/update-author-by-id/{authorId}")
    @Operation(summary = "Update Author By Id")
    ResponseEntity<AuthorResponse<Author>> updateAuthorById(@RequestBody AuthorRequest authorRequest,Integer authorId){
        Integer  storeAuthorId =  authorService.updateAuthorById(authorRequest,authorId);
        if(storeAuthorId != null){
            AuthorResponse<Author> response  =  AuthorResponse.<Author>builder()
                    .message("Successfully updated author")
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;

    }

}
