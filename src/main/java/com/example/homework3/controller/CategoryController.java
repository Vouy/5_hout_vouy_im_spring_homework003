package com.example.homework3.controller;

import com.example.homework3.model.entity.Author;
import com.example.homework3.model.entity.Category;
import com.example.homework3.model.request.AuthorRequest;
import com.example.homework3.model.request.CategoryRequest;
import com.example.homework3.model.response.AuthorResponse;
import com.example.homework3.model.response.CategoryRespose;
import com.example.homework3.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private  final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/get-all-category")
    @Operation(summary = "Get All Category")
    ResponseEntity<CategoryRespose<List<Category>>> getAllCategory(){
        CategoryRespose<List<Category>> respose = CategoryRespose.<List<Category>>builder()
                .message("Successfully fetched categories")
                .payload(categoryService.getAllCategory())
                .httpStatus(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(respose);
    }

    @GetMapping("/get-category-by-id/{categoryId}")
    @Operation(summary = "Get Category By Id")
    ResponseEntity<CategoryRespose<Category>> getCategoryById(@PathVariable("categoryId")  Integer categoryId){
        if(categoryService.getCategoryById(categoryId) != null){
            CategoryRespose<Category> respose = CategoryRespose.<Category>builder()
                    .message("Successfully fetched category")
                    .payload(categoryService.getCategoryById(categoryId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return   ResponseEntity.ok(respose);
        }
        return null;

    }

    @PostMapping("/add-new-category")
    @Operation(summary = "Add New Category")
    ResponseEntity<CategoryRespose<Category>> addNewAuthor(@RequestBody CategoryRequest categoryRequest){
        Integer storeCategoryId = categoryService.addNewCategory(categoryRequest);
        if(storeCategoryId != null){
            CategoryRespose<Category> response = CategoryRespose.<Category>builder()
                    .message("Successfully added Category")
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @DeleteMapping("/delete-category-by-id/{categoryId}")
    @Operation(summary = "Delete Category By Id")
    ResponseEntity<CategoryRespose<String>>  deleteCategoryById(@PathVariable("categoryId") Integer categoryId){
        CategoryRespose<String>  respose = null;
        if(categoryService.deleteCategoryById(categoryId) == true){
            respose = CategoryRespose.<String>builder()
                    .message("Successfully deleted category")
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(respose);
        }
        return null;
    }

    @PutMapping("/update-category-by-id/{categoryId}")
    @Operation(summary = "Update Category By Id")
    ResponseEntity<CategoryRespose<Category>>  updateCategoryById(@RequestBody CategoryRequest categoryRequest,Integer categoryId){
        Integer  storeUpdateId  = categoryService.updateCategoryById(categoryRequest,categoryId);
        if(storeUpdateId != null){
            CategoryRespose<Category> respose  = CategoryRespose.<Category>builder()
                    .message("Successfully updated category")
                    .payload(categoryService.getCategoryById(storeUpdateId))
                    .httpStatus(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(respose);
        }
        return null;
    }

}
