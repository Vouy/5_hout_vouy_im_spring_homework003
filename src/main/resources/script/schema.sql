-- CREATE DATABASE bookcontrol;

-- CREATE TABLE authors(
--     author_id  SERIAL primary key,
--     author_name VARCHAR(50),
--     gender VARCHAR(50)
-- )

-- CREATE  TABLE categories(
--     id SERIAL primary key,
--     category_name  VARCHAR(255)
-- )

-- CREATE  TABLE books(
--     id  SERIAL primary key,
--     title VARCHAR(225),
--     published_date TIMESTAMP,
--     author_id INT REFERENCES authors(author_id)
--                    ON DELETE CASCADE
--                    ON UPDATE CASCADE
-- )

-- CREATE  TABLE book_details(
--     id  SERIAL  primary key,
--     book_id  INT REFERENCES books(id)
--         ON DELETE CASCADE  ON UPDATE CASCADE ,
--     category_id INT REFERENCES categories(id)
--         ON DELETE CASCADE  ON UPDATE CASCADE
-- )

-- SELECT DISTINCT c.id , c.category_name FROM categories c
--                 INNER JOIN book_details  d ON c.id = d.category_id
--             WHERE d.book_id = 1